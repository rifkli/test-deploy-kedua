import React, { Component } from 'react';
import Frame from './Frame';

import Result from './Result';

class App extends Component {
  state = {
    result: ''
  }
  klickButton = klickName=>{
    
    if(klickName === "=" ){
      this.calculate();
    }else if(klickName === "C"){
      this.reset();
    }else if(klickName === "%" && klickName === "="){
      this.persen();
    }
    else if(klickName=== "dell"){
      this.delete();
    }
    else 
    this.setState({
      result: this.state.result + klickName
    });
  };
  calculate = () =>{
    this.setState({
      result: eval(this.state.result)
    });
  };
  persen =()=>{(
    this.setState({
      result: this.state.result /100
    })
  )}
  reset =() =>{
    this.setState({
      result:''
    })
  }
  delete =() =>{
    this.setState({
      result: this.state.result.slice(0, -1)
    })
  }
  
  render() {
    return (
      <div>
        <Result result={this.state.result}/>  
        <Frame klickButton={this.klickButton}/>
      </div>  
    );
  }
}

export default App
