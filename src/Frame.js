import React, { Component } from 'react'; 

class Frame extends Component {
    klickButton = (e) => {
        this.props.klickButton(e.target.value)
    }

    render() {
        return (
            <>
            <div class="row">
                <button name ="C" onClick={this.klickButton} value="C" class="operator">C</button>
                <button name ="-" onClick={this.klickButton} value="-" class="operator">+/-</button>
                <button name ="%" onClick={this.klickButton} value="%" class="operator">%</button>
                <button name ="/" onClick={this.klickButton} value="/" class="operator">÷</button>
            </div>
            <div class="row">
                <button name ="7" onClick={this.klickButton} value="7">7</button>
                <button name ="8" onClick={this.klickButton} value="8">8</button>
                <button name ="9" onClick={this.klickButton} value="9">9</button>
                <button name ="*" onClick={this.klickButton} value="*" class="operator">*</button>
            </div>
            <div class="row">
                <button name ="4" onClick={this.klickButton} value="4">4</button>
                <button name ="5" onClick={this.klickButton} value="5">5</button>
                <button name ="6" onClick={this.klickButton} value="6">6</button>
                <button name ="-" onClick={this.klickButton} value="-" class="operator">-</button>
            </div>
            <div class="row">
                <button name ="1" onClick={this.klickButton} value="1">1</button>
                <button name ="2" onClick={this.klickButton} value="2">2</button>
                <button name ="3" onClick={this.klickButton} value="3">3</button>
                <button name ="+" onClick={this.klickButton} value="+" class="operator">+</button>
            </div>
            <div class="row">
                <button name ="0" onClick={this.klickButton} value="0">0</button>
                <button name ="." onClick={this.klickButton} value=".">.</button>
                <button name ="dell" onClick={this.klickButton} value="dell">dell</button>
                <button name ="=" onClick={this.klickButton} value="=" class="operator">=</button>    
            </div>
            </>
        );
    }
}

export default Frame;